//
//  VoteViewController.swift
//  Avalon
//
//  Created by ChenZheng-Yang on 2020/4/14.
//  Copyright © 2020 ChenCheng-Yang. All rights reserved.
//

import UIKit

class VoteViewController: UIViewController {
    
    // MARK: - Properties
    fileprivate(set) var voteCount: Int = 0
    fileprivate(set) var voteResult: [Bool] = []
    
    @IBOutlet weak var numberOfVoteLabel: UILabel!
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

// MARK: - Methods
extension VoteViewController {
    fileprivate func updateNumberOfVoteLabel() {
        numberOfVoteLabel.text = "Number of voter:\(voteCount)"
    }
}

// MARK: - Actions
extension VoteViewController {
    @IBAction func didPressVoteButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "請投票", message: "決定任務成功失敗", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "成功", style: .default, handler: { [weak self] _ in
            guard let this = self else { return }
            this.voteCount += 1
            this.updateNumberOfVoteLabel()
            this.voteResult.append(true)
        }))
        
        alert.addAction(UIAlertAction(title: "失敗", style: .default, handler: { [weak self] _ in
            guard let this = self else { return }
            this.voteCount += 1
            this.updateNumberOfVoteLabel()
            this.voteResult.append(false)
        }))
        alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func didPressResultButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "完成投票？", message: "請確認所有人投票", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "確定", style: .default, handler: { [weak self] _ in
            guard let this = self else { return }
            var message: String = ""
            if this.voteResult.contains(false) {
                let failCount = this.voteResult.filter({ $0 == false }).count
                message = "失敗，有\(failCount)張失敗票"
            } else {
                message = "成功"
            }
            this.voteCount = 0
            this.updateNumberOfVoteLabel()
            this.voteResult.removeAll()
            let alert2 = UIAlertController(title: "任務結果", message: message, preferredStyle: .alert)
            alert2.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            this.present(alert2, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
