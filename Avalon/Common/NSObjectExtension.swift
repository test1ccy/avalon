//
//  NSObjectExtension.swift
//  Avalon
//
//  Created by ChenZheng-Yang on 2020/1/28.
//  Copyright © 2020 ChenCheng-Yang. All rights reserved.
//

import Foundation

extension NSObject {

    /**
    Only the `ClassName` part of `ModuleName.ClassName` i.e. no `ModuleName`.
    - Note:
    `fileprivate` classes have more complex short names.
    */
    class var shortName: String {
        return String(describing: self)
    }
}
