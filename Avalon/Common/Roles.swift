//
//  Roles.swift
//  Avalon
//
//  Created by ChenZheng-Yang on 2020/1/28.
//  Copyright © 2020 ChenCheng-Yang. All rights reserved.
//

import Foundation
import UIKit

enum Roles: String {
    case mordred
    case mormna
    case assassin
    case badPeople
    case oberon
    
    case merlin
    case pecival
    case goodGuys
    
    var name: String {
        switch self {
        case .mordred:
            return "莫德雷德（大魔王）"
        case .mormna:
            return "魔甘娜（假梅林）"
        case .assassin:
            return "刺客（壞人）"
        case .badPeople:
            return "壞人"
        case .oberon:
            return "奧柏倫（傻逼壞人）"
        
        case .merlin:
            return "梅林（好人領導）"
        case .pecival:
            return "派西維爾（西瓜皮）"
        case .goodGuys:
            return "好人"
        }
        
    }
    
    var isGoodGuys: Bool {
        switch self {
        case .merlin,
             .pecival,
             .goodGuys:
            return true
        default:
            return false
        }
    }
    
    var image: UIImage? {
        return UIImage(named: self.rawValue)
    }
}

// MARK: - Methods
extension Roles {
    
}
