//
//  Game.swift
//  Avalon
//
//  Created by ChenZheng-Yang on 2020/1/28.
//  Copyright © 2020 ChenCheng-Yang. All rights reserved.
//

import Foundation

class Game {
    
    // MARK: - Properties
    static let shared = Game()
    
    var player: [Roles] = []
    var taskSuccessArray: [Bool] = Array(repeating: false, count: 5)
}

// MARK: - Methods
extension Game {
    func reset() {
        player = []
        taskSuccessArray = Array(repeating: false, count: 5)
    }
}
