//
//  Common.swift
//  Avalon
//
//  Created by ChenZheng-Yang on 2020/1/28.
//  Copyright © 2020 ChenCheng-Yang. All rights reserved.
//

import Foundation
import UIKit

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height
