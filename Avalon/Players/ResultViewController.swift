//
//  ResultViewController.swift
//  Avalon
//
//  Created by ChenZheng-Yang on 2020/1/28.
//  Copyright © 2020 ChenCheng-Yang. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    
    @IBOutlet weak var switch1: UISwitch!
    @IBOutlet weak var switch2: UISwitch!
    @IBOutlet weak var switch3: UISwitch!
    @IBOutlet weak var switch4: UISwitch!
    @IBOutlet weak var switch5: UISwitch!
    
    fileprivate(set) var switchs: [UISwitch] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        switchs = [switch1, switch2, switch3, switch4, switch5]
        for (index, s) in switchs.enumerated() {
            s.isOn = Game.shared.taskSuccessArray[index]
        }
    }

    @IBAction func didSwitch(_ sender: UISwitch) {
        Game.shared.taskSuccessArray[sender.tag] = sender.isOn
    }
}
