//
//  NumberOfPlayersViewController.swift
//  Avalon
//
//  Created by ChenZheng-Yang on 2020/1/27.
//  Copyright © 2020 ChenCheng-Yang. All rights reserved.
//

import UIKit

class NumberOfPlayersViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var voteCountButton: UIBarButtonItem!
    
    fileprivate var roles: [Roles] = [
        .merlin,
        .pecival,
        .goodGuys,
        .goodGuys,
        .goodGuys,
        .goodGuys,
        .goodGuys,
        .mordred,
        .mormna,
        .assassin,
        .badPeople,
        .badPeople,
        .badPeople,
        .oberon
    ]
    
    fileprivate var selectedArray: [Bool] = []
    

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupCollectionView()
        setupSelectedArray()
    }
    
    // MARK: Setup
    fileprivate func setupCollectionView() {
        collectionView.register(UINib(nibName: RoleCollectionViewCell.shortName, bundle: nil), forCellWithReuseIdentifier: RoleCollectionViewCell.shortName)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    fileprivate func setupSelectedArray() {
        selectedArray = Array(repeating: false, count: roles.count)
    }
    
}

// MARK: - UICollectionViewDelegate
extension NumberOfPlayersViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let isSelected = selectedArray[indexPath.item]
        selectedArray[indexPath.item] = !isSelected
        collectionView.reloadData()
    }
}

// MARK: - UICollectionVIewDataSource
extension NumberOfPlayersViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return roles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RoleCollectionViewCell.shortName, for: indexPath) as? RoleCollectionViewCell else {
            return UICollectionViewCell()
        }
        let role = roles[indexPath.item]
        let isSelect = selectedArray[indexPath.item]
        cell.imageView.image = role.image
        cell.titleLabel.text = role.name
        cell.selectedImageView.isHidden = !isSelect
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension NumberOfPlayersViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = UIScreen.main.bounds.width * 0.47
        return CGSize(width: cellWidth, height: cellWidth * 1.4)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let horizontalPadding: CGFloat = 20
        return UIEdgeInsets(top: 0, left: horizontalPadding, bottom: 0, right: horizontalPadding)
    }

}
// MARK: - Actions
extension NumberOfPlayersViewController {
    @IBAction func didPressNextButton(_ sender: UIButton) {
        for (index, selected) in selectedArray.enumerated() {
            if selected {
                Game.shared.player.append(roles[index])
            }
        }
        performSegue(withIdentifier: "GoSelectRoleViewController", sender: nil)
    }
    
    @IBAction func didPressRefreshButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "確認重置?", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "確定", style: .default, handler: { [weak self] _ in
            guard let this = self else { return }
            Game.shared.reset()
            this.setupSelectedArray()
            this.collectionView.reloadData()
        }))
        alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - Navigation
extension NumberOfPlayersViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SelectRoleViewController {
            vc.configure()
        }
    }
}
