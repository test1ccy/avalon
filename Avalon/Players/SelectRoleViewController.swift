//
//  SelectRoleViewController.swift
//  Avalon
//
//  Created by ChenZheng-Yang on 2020/1/28.
//  Copyright © 2020 ChenCheng-Yang. All rights reserved.
//

import UIKit

class SelectRoleViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var button: UIButton!
    
    fileprivate var playerCount: Int {
        return Game.shared.player.count
    }
    
    fileprivate var randomRoles: [Roles] = []
    
    fileprivate(set) var isOpenInfo: Bool = false
    fileprivate(set) var currentSelectdRow: Int = 0
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupPIckekView()
    }
    
    // MARK: - Setup
    func configure() {
        
        
        var roles = Game.shared.player
        while roles.count != 0 {
            let index = Int.random(in: 0 ..< roles.count)
            randomRoles.append(roles[index])
            roles.remove(at: index)
        }
    }
    
    fileprivate func setupPIckekView() {
        pickerView.dataSource = self
        pickerView.delegate = self
    }
}

// MARK: - Methdos
extension SelectRoleViewController {
    
}

// MARK: - Actions
extension SelectRoleViewController {
    @IBAction func didPressButton(_ sender: UIButton) {
        guard !isOpenInfo else {
            isOpenInfo = !isOpenInfo
            imageView.image = nil
            titleLabel.text = ""
            return
        }
        let alert = UIAlertController(title: "確定是該名玩家？", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "確定", style: .default, handler: { [weak self] _ in
            guard let this = self else { return }
            this.isOpenInfo = !this.isOpenInfo
            let role = this.randomRoles[this.currentSelectdRow]
            this.imageView.image = role.image
            this.titleLabel.text = role.name
                   
        }))
        alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
       present(alert, animated: true, completion: nil)
    }
}

// MARK: - IPickerViewDelegate
extension SelectRoleViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentSelectdRow = row
    }
}

// MARK: - UIPickerViewDataSource
extension SelectRoleViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return playerCount
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "玩家\(row + 1)"
    }
}
